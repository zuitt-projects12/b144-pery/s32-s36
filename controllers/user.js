const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/*
	Check if the email already exists.
	Business Logic
	1. User find() method to find duplicate emails.
	2. Error-handling. If no duplicate found, return false, else return true.

	IMPORTANT NOTE: It is best practice to return a result is to use a boolean, or return an object/array of object, because string is limited in our backend, and can't be connected to our frontend.
*/
module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		}
		else{
			// No duplicate email found.
			return false;
		} // end else
	}); // end return
}; // end module.exports.checkEmailExists

/*
	User registration
	Business Logic:
	1. Create a new User object using the mongoose model and the info from the request body.
	2. Make sure that the password is encrypted.
	3. Save the new user to the database.
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password. 
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	}); // end let newUser

	// Save the data
	return newUser.save().then((user, err) => {
		// If registration failed
		if(err){
			return false;
		}
		else{
			// User registration is successful
			return true;
		} // end else
	}); // end newUser.save().then()
}; // end module.exports.registerUser

/*
	User authentication
	Business Logic:
	1. Check if the user's email exists in our database. 
		- If user does not exist, return false.
	2. If user exists, compare the password provided in the login form with the password stored in the database.
	3. Generate/return a jsonwebtoken if the user is successfully loggedin and return false if not.
*/
module.exports.loginUser = (reqBody) => {
	// findOne() method returns the FIRST record in the collection that matches the search criteria.
	// We use findOne() instead of find() method which returns all records that match the search criteria.
	return User.findOne({email: reqBody.email}).then(result => {
		// User does not exist
		if(result === null){
			return false;
		}
		else{
			// User exists
			// The compareSync() method is used to compare a non-encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the password match the result of the above code is true.
			if(isPasswordCorrect){
				// Generate access token
				// The mongoose toObject() method converts the mongoose obejct into a plain javascript object.
				return { accessToken: auth.createAccessToken(result.toObject()) };
			}
			else{ 
				// Password do not match.
				return false;
			} // end else if(isPasswordCorrect)
		} // end else
	}); // end User.findOne()  
}; // end module.exports.loginUser

/*
	Retrieving a specific user
	Business Logic:
	1. Find the user with the ID specified in the URL (using findByID() function).
	2. Display the data EXCEPT THE PASSWORD.
*/
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id, {password: 0}).then((result, err) => {
		if(err || reqBody.id === ""){
			return false;
		}
		else{	
			if(result === null){
				return false;
			}
			else{
				return result;
			} // end else if(result === null)
		} // end else if (err || reqBody.id === "")
	}); // end User.findOne().then
}; // end module.exports.getProfile

// Enrolling a user to a specific course
/*
	Business Logic:
	1. Find the document in the database using the user's ID.
		TODO:
		- Find the course using the course ID indicated.
		- If course is not active, then return false.
		- If course is active, then save it to the database.
	2. Add the course ID to the user's enrollment array.
		TODO:
		- If course is not active, then return false.
		- If course is active, then save it to the database.
	3. Update the document in the MongoDB Atlas/Robo3t Database.
*/
module.exports.enroll = async (data) => {
	// Add the course ID in the enrollments array of the user.
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Check if course is active
		return Course.findById(data.courseId).then((course, err) => {
			if(err){
				// error handling
				return false;
			} // end if(err)
			else{
				if(course.isActive){
					// Adds the courseId in the user's enrollments array.
					user.enrollments.push({courseId: data.courseId});

					// Saves the updated user's information in the database.
					return user.save().then((user, err) => {
						if(err){
							return false;
						}
						else{
							return true;
						}
					}); // end return user.save().then()
				} // end if (course.isActive)
				else{
					return false;
				} // end else if (course.isActive)
			} // end else if (err)
		}); // end return Course.findById
	}); // end let isUserUpdated

	// Add the user ID in the enrollees array of the course.
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Check if course is active
		if(course.isActive){
			// Adds the userId in the course's enrollees array. 
			course.enrollees.push({userId: data.userId});

			// Saves the updated course information in the database.
			return course.save().then((course, err) => {
				if(err){
					return false;
				}
				else{
					return true;
				}
			}); // end return course.save().then()
		} // end if(course.isActive)
		else{
			return false;
		} // end else if (course.isActive)
	}); // let isCourseUpdated

	// Condition that will check if the user and course documents have been updated.
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	else{
		return false;
	}
}; // end module.exports.enroll