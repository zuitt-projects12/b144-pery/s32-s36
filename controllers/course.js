const Course = require("../models/Course");
const User = require("../models/User");
const auth = require("../auth");

/*
	Adding a course with validation.
	Business Logic:
	1. Find the user indicated in the userData in the users collection.
	2. If error, return false.
	3. If not, then determine if the user given is an admin or not.
		- If it is an admin, then save the course from the request body.\
		- If not, return false. 
*/
module.exports.addCourse = (reqBody, userData) => {
	return User.findOne({email: userData.email}).then((result, err) => {
		if(err){
			return false;
		}
		else{
			if(result.isAdmin){
				let newCourse = new Course({
					name: reqBody.name,
					description: reqBody.description,
					price: reqBody.price
				}); // end let newCourse = new Course()

				// Save the course
				return newCourse.save().then((course, err) => {
					// If course creation failed.
					if(err){
						return false;
					}
					// If course creation successful.
					else{
						return true;
					} // end else if(err)
				}); // end return Course.save().then()
			}
			else{
				return false;
			} // end else if (result.isAdmin)
		} // end else if(err)
	}); // end return User.findOne().then()
} // end module.exports.addCourse

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result); // end return Course.find().then
}; // end module.exports.getAllCourses

// Retrieve all active courses
module.exports.getAllActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	}); // end return Course.find().then()
}; // end return module.exports.getAllCourses

// Retrieve a specific course.
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	}); // end return Course.findById().then()
}; // end return module.exports.getCourse

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	} // end let updatedCourse
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course , err) => {
		if(err){
			return false;
		}
		else{
			return true;
		}
	}); // end return Course.findByIdAndUpdate().then()
}; // end return module.exports.updateCourse

// Archive a course
module.exports.archiveCourse = (reqParams) => {
	return Course.findByIdAndUpdate(reqParams.courseId, {isActive: false}).then((result, err) => {
		if(err){
			return false;
		}
		else{
			return true;
		}
	}); // end return Course.findByIdAndUpdate().then()
}; // end module.exports.archiveCourse

// Archive a course
module.exports.activateCourse = (reqParams) => {
	return Course.findByIdAndUpdate(reqParams.courseId, {isActive: true}).then((result, err) => {
		if(err){
			return false;
		}
		else{
			return true;
		}
	}); // end return Course.findByIdAndUpdate().then()
}; // end module.exports.archiveCourse