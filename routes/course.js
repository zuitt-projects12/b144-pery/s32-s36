const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course with validation.
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.addCourse(req.body, userData).then(result => res.send(result));
});

// Route for retrieving all courses.
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
});

// Route for retrieving all ACTIVE courses.
router.get("/", (req, res) => {
	courseController.getAllActiveCourses().then(result => res.send(result));
});

// Route for retrieving a specific course.
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params.courseId).then(result => res.send(result));
});

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});

// Route for archiving a course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(result => res.send(result));
});

// Route for activating a course
router.patch("/:courseId/activate", auth.verify, (req, res) => {
	courseController.activateCourse(req.params).then(result => res.send(result));
});

module.exports = router;