const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/user");

// Routes for checking if the user's email already exists in the database.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Routes for user registration.
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

// Routes for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

// Routes for retrieving a user by ID.
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(userData).then(result => res.send(result));
});

// Routes to enroll a user to a course
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id,
		courseId: req.body.courseId,
	}

	userController.enroll(data).then(result => res.send(result));
});

module.exports = router;