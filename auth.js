// JSON web tokens are standard for sending info between our app in a secure manner. It will allow us to gain access to methods that will help us to create a JSON web token.

const jwt = require("jsonwebtoken");
const secret = "CrushAkoNgCrushKo";

// JWT is a way of securely passing info from the server to the frontend or to the other parts of the server.
// Info is kept secure throught the use of the secret code. Only the system that knows the secret code that can decode the encrypted info.

// Token creation
// Analogy: Pack the gift and provide a lock with the secret code as the key.
module.exports.createAccessToken = (user) => {
	// Data will be received from the registration form. When the user logged in, a token will be created with the user's information.
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}; // end const data

	// Generates the token using the form data and the secret code with no addistional options provided.
	return jwt.sign(data, secret, {});
}; // end module.exports.createAccessToken

// Token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		// console.log(token);
		// "Bearer: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYWYwMzBiM2ZjNDQyOTkwODkxOTA5NyIsImVtYWlsIjoiamFuZUBzb21ldGhpbmcuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTYzOTAxMDYyOH0.b0mUqWaxyTCrXMcmJ9D0EcMsbAw_hFqyYRtSSkXkgWo"
		token = token.slice(7, token.length);

		// Validate the token using the "verify" method.
		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if(err){
				return res.send({auth: "failed"});
			}
			else{
				// The next() method allows the application to proceed with the next middleware function/callback function in the route.
				next();
			} // end else if(err)
		}); // end jwt.verify()
	} // end if(typeof token !== "undefined")
	
	// Token does not exist
	else{
		return res.send({auth: "failed"});
	} // else if(typeof token !== "undefined")
}; // end module.exports.verify

// Token decryption
module.exports.decode = (token) => {
	// Token received and is not undefined
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}
			else{
				// "Decode" method is used to obtain the informaiton from the JWT.
				// "{complete: true}" option allows us to return additional information from the JWT.
				// Returns an object with access to the "payload" property which contains user information stored when the payload was generated.
				return jwt.decode(token, {complete: true}).payload;
			} // end else if(err)
		}); // end return jwt.verify() 
	} // end if(typeof token !== "undefined")
	else{
		return null;
	} // end else if(typeof token !== "undefined")
}; // end module.exports.decode