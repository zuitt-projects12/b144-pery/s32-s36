// Required modules
const express = require("express");
const mongoose = require("mongoose");
// Allows us to control app's Cross-Origin Resource Sharing settings.
const cors = require("cors");
// Routes
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

// Server setup
const app = express();
const port = 3001;
// Allows all resources/origin to access our backend application.
// Enable all CORS
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Defines the "/api/users" string to be included for all routes definbed in the "user" route file.
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

// DB connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.dwfxo.mongodb.net/batch144-booking-system?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the MongoDB Atlas"));

app.listen(port, () => console.log(`Server running at port ${port}`));